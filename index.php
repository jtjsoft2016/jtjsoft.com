<?php require "html_header.php"; ?>
<div id="loading">
  <img src="img/load.gif">
</div>
<div id="nav">
  <!-- <img src="img/logo.png"> -->
</div>
<div id="top"></div>
<div id="background">
</div>
<div class="uk-text-center uk-vertical-align" id="main1">
  <div class="uk-vertical-align-middle">
    <h1 style="font-size:50px;margin-bottom:100px" data-uk-scrollspy="{cls:'uk-animation-slide-left', repeat: true, delay:0}">JTJSOFT</h1>
    <p class="uk-text-large" style="color:white;margin:0;text-shadow:0px 0px 10px black;" id="type">

    </p>
  </div>
  <div class="uk-container uk-container-center uk-text-center">
    <a href="#main2" class="bottom_bt uk-icon-button uk-icon-arrow-circle-down" data-uk-smooth-scroll	></a>
  </div>
</div>
<div class="uk-container-center uk-text-center uk-vertical-align" id="main2">
  <div class="uk-vertical-align-middle" style="width:calc(100% - 100px);height:auto;" id="main2_">
    <h1 style="font-size:50px;margin-bottom:100px" data-uk-scrollspy="{cls:'uk-animation-slide-left', repeat: true, delay:0}">Service</h1>
    <div class="uk-grid">
      <div class="uk-width-large-1-2">
        <div class="uk-grid">
          <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1"  data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:0}">
            <div class="uk-panel uk-panel-box">
              <i class="uk-icon-large uk-icon-question"></i>
              <h2 class="uk-margin-top-remove h2st">Guessing Me<br>Project</h2>
              <p class="h2st">게싱미 프로젝트</p>
            </div>
          </div>
          <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1"  data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:300}">
            <div class="uk-panel uk-panel-box">
              <i class="uk-icon-large uk-icon-pencil"></i>
              <h2 class="uk-margin-top-remove h2st">Broad<br>School</h2>
              <p class="h2st">브로드스쿨</p>
            </div>
          </div>
        </div>
      </div>
      <div class="uk-width-large-1-2">
        <div class="uk-grid">
          <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1"  data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:600}">
            <div class="uk-panel uk-panel-box">
              <i class="uk-icon-large uk-icon-rss"></i>
              <h2 class="uk-margin-top-remove h2st">SELFREE<br>WEB</h2>
              <p class="h2st">셀프리웹</p>
            </div>
          </div>
          <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1"  data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:900}">
            <div class="uk-panel uk-panel-box">
              <i class="uk-icon-large uk-icon-wechat"></i>
              <h2 class="uk-margin-top-remove h2st">JTJ<br>Page</h2>
              <p class="h2st">jtjpage</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="uk-container uk-container-center uk-text-center">
    <a href="#main3" class="bottom_bt uk-icon-button uk-icon-arrow-circle-down" data-uk-smooth-scroll	data-uk-scrollspy="{cls:'uk-animation-slide-bottom', repeat: true, delay:1200}"></a>
  </div>
</div>
<div class="uk-container-center uk-text-center uk-vertical-align" id="main3">
  <div class="uk-vertical-align-middle uk-container-center" style="width:50%" id="main3_">
    <h1 style="font-size:50px;margin-bottom:100px" data-uk-scrollspy="{cls:'uk-animation-slide-left', repeat: true, delay:0}">Customer Center</h1>

    <div class="uk-grid">
      <div class="uk-width-large-1-1">
        <div class="uk-grid">
          <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1"  data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:0}">
            <div class="uk-panel uk-panel-box">
              <i class="uk-icon-large uk-icon-facebook-official"></i>
              <h2 class="uk-margin-top-remove h2st">Facebook</h2>
              <p class="h2st"><a href="//facebook.com/jtjsoft">JTJSOFT 페이스북 페이지</a></p>
            </div>
          </div>
          <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1"  data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:300}">
            <div class="uk-panel uk-panel-box">
              <i class="uk-icon-large uk-icon-twitter-square"></i>
              <h2 class="uk-margin-top-remove h2st">Twitter</h2>
              <p class="h2st"><a href="https://twitter.com/jtjsof">JTJSOFT 트위터</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="uk-width-large-1-1">
        <div class="uk-grid">
          <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1"  data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:600}">
            <div class="uk-panel uk-panel-box">
              <img src="img/story_symbol.png" style="height:35px;width:30px;margin-bottom:10px"></i>
              <h2 class="uk-margin-top-remove h2st">KaKaoStory</h2>
              <p class="h2st"><a href="https://story.kakao.com/ch/jtjsoft">JTJSOFT 카카오스토리</a></p>
            </div>
          </div>
          <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1"  data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:900}">
            <div class="uk-panel uk-panel-box">
              <i class="uk-icon-large uk-icon-comments"></i>
              <h2 class="uk-margin-top-remove h2st">KaKaoTalk</h2>
              <p class="h2st"><a href="http://plus.kakao.com/home/@%EC%A0%9C%EC%9D%B4%ED%8B%B0%EC%A0%9C%EC%9D%B4%EC%86%8C%ED%94%84%ED%8A%B8">JTJSOFT 옐로우 아이디</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="uk-container uk-container-center uk-text-center">
    <a href="#main4" class="bottom_bt uk-icon-button uk-icon-arrow-circle-down" data-uk-smooth-scroll	data-uk-scrollspy="{cls:'uk-animation-slide-bottom', repeat: true, delay:1200}"></a>
  </div>
</div>
<div id="footer" class="uk-text-center uk-vertical-align">
  <div class="uk-vertical-align-middle">
    <p style="color:white">Made by <a href="//gdb.kr">GDB.kr</a>, <a href="https://github.com/SuhwanCha">Suhwan Cha</a> 2016</p>
    <p style="color:white;margin-bottom:0">CopyRight 2016, <a href="//jtjsoft.com">JTJSOFT</a> ALL rights reserved</p>
  </div>
</div>

<div class="uk-container-center uk-text-center uk-vertical-align" id="main4">
  <div class="uk-vertical-align-middle" style="width:calc(100% - 100px);">
    <h1 style="font-size:50px;margin-bottom:100px" data-uk-scrollspy="{cls:'uk-animation-slide-left', repeat: true, delay:0}">Members</h1>
    <div class="uk-grid">
      <div class="uk-width-large-1-3 uk-width-small-1-1 uk-text-center" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:000}">
        <div class="uk-panel uk-panel-box profile">
          <h1 class="h2st">장태진</h1>
          <h2 class="uk-margin-top-remove h2st">Jang Tae-jin</h2>
          <p class="h2st">JTJSOFT Web Developer</p>
        </div>
      </div>
      <div class="uk-width-large-1-3 uk-width-small-1-1" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:300}">
        <div class="uk-panel uk-panel-box profile">
          <h1 class="h2st">양중효</h1>
          <h2 class="uk-margin-top-remove h2st">Yang Jung-hyo</h2>
          <p class="h2st">Web Developer</p>
        </div>
      </div>
      <div class="uk-width-large-1-3 uk-width-small-1-1 " data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:600}">
        <div class="uk-panel uk-panel-box profile">
          <h1 class="h2st">차수환</h1>
          <h2 class="uk-margin-top-remove h2st">Cha Su-hwan</h2>
          <p class="h2st">GDB.kr Web Developer</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="gototop">
  <a class="uk-icon-button uk-icon-arrow-up" href="#top" data-uk-smooth-scroll="{offset: 72}"></a>
</div>
</body>
<script>  $("#main2").css("min-height", $("#main2_").height());</script>
<script>  $("#main3").css("min-height", $("#main3_").height());
  $(document).ready(function(){
    $("*").css("font-faimly","NanumGothic");
  })
</script>
</html>
