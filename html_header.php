<!doctype html>
<html lang="ko">
<head>
	<title>:: JTJSOFT ::</title>
	<!-- <link href="https://fonts.googleapis.com/earlyaccess/notosanskr.css" rel="stylesheet" type="text/css"> -->
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/trianglify/0.4.0/trianglify.min.js"></script> -->
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.25.0/js/components/accordion.min.js"></script> -->
	<!-- <script src="js/components/slideshow.js"></script> -->
	<!-- <script src="js/components/slideshow-fx.js"></script> -->
	<!-- <script src="js/components/parallax.js"></script> -->
	<!-- <script src="js/components/tooltip.js"></script> -->
	<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.25.0/css/components/accordion.min.css" rel="stylesheet" type="text/css"> -->
	<!-- <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
	<link href="./css/uikit.css" rel="stylesheet" type="text/css">
	<link href="./css/style2.css" rel="stylesheet" type="text/css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.25.0/js/uikit.min.js"></script>
	<script src="js/components/sticky.js"></script>
	<script src="type/js/typed.js" type="text/javascript"></script>
	<script src="script.js" type="text/javascript"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#CBB1A4">
	<meta property="og:title" content=":: JTJSOFT.COM ::">
	<meta property="og:url" content="http://jtjsoft.com/">
	<meta property="og:image" content="http://jtjsoft.com/img/thumb.png">
	<meta property="og:description" content="JTJSOFT 메인페이지입니다.">
	<meta name="content-language" content="kr">
	<meta name="keyword" content="제이티제이소프트, JTJSOFT, 장태진, jtjsoft.com, gdb.kr, asdqwe2e">
	<meta name="description" content="JTJSOFT.com">
	<meta name="author" content="Suhwan Cha">
	<meta name="reply-to" content="jtjsoft@naver.com">
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Imagetoolbar" content="no">
</head>
<body>
